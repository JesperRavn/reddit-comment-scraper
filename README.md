# Reddit Comment Scraper #
A script to scrape comments from a a given subreddit

## Setup guide ##
###Install praw###
```
#!bash

pip install praw
```
###Populate the praw.ini###

* Go to [https://www.reddit.com/prefs/apps/](https://www.reddit.com/prefs/apps/)
* Press "create app"
* Add a name
* Select "Script"
* Add description
* Add http://localhost:8080/ where asked about URL
* Click "Create App"
* Now you should be able to see your app and the client_id and client_secret
* Insert client_id and client_secret into the praw.ini file