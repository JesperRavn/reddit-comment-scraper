import json
import datetime
import praw
from praw.models import MoreComments


class Submission:
    """
    Made to store all the relevant info about the a submission(post), 
    comments is a flat list containing all the submission comments
    in the form of the class Comments
    """
    def __init__(self, created, gilded, permalink, title, score, comments):
        self.created = created
        self.gilded = gilded
        self.permalink = permalink
        self.title = title
        self.score = score
        self.comments = comments


class Comment:
    """
    Made to store all the relevant info about the a comment
    """
    def __init__(self, created, gilded, body, score, link_id):
        self.created = created
        self.gilded = gilded
        self.body = body
        self.score = score
        self.link_id = link_id


class Subreddit:
    """
    Main class that stores all the submissions and relevant metadata about the subreddit.
    The botname is used to retrieve data from the praw.ini file.
    Make sure to populate the praw.ini file with client_id, and client_secret
    """
    def __init__(self, subreddit, botname):
        self.subreddit = subreddit
        self.botname = botname
        self.timestamp = "{:%H:%M:%S %d-%m-%y}".format(datetime.datetime.now())
        self.data = []

    def retrieve_data(self):
        reddit = praw.Reddit(self.botname)

        subreddit = reddit.subreddit(self.subreddit)

        for submission in subreddit.top('all'):
            submission.comments.replace_more(limit=0)
            submission_comments = []
            for sub_comment in submission.comments.list():
                submission_comments.append(Comment(sub_comment.created,
                                                   sub_comment.gilded,
                                                   sub_comment.body,
                                                   sub_comment.score,
                                                   sub_comment.link_id))

            self.data.append(Submission(submission.created,
                                        submission.gilded,
                                        submission.permalink,
                                        submission.title,
                                        submission.score,
                                        submission_comments))

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, 
            sort_keys=True, indent=4)




def main():
    subreddit = Subreddit("ethereum","comment_scraper")
    subreddit.retrieve_data()
    subreddit.data

    with open('data.json', 'w') as outfile:
        outfile.write(subreddit.toJSON())

if __name__ == '__main__':
    main()

